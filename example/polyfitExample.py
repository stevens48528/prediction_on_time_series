import json
import datetime as dt
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)

minutes = mdates.MinuteLocator(interval=30) # every 30 minutes
minutes_fmt = mdates.DateFormatter('%M')
hours = mdates.HourLocator()
hours_fmt = mdates.DateFormatter('%H')
days_fmt = mdates.DateFormatter('%d')
days = mdates.DayLocator()
months = mdates.MonthLocator()
months_fmt = mdates.DateFormatter('%m')
years = mdates.YearLocator()
years_fmt = mdates.DateFormatter('%Y')

x = []
AirTemp = []
count = 0

json_file = open('sensor.json')
data = json.load(json_file)
for item in data:
    if(int(item['Gid']) > 193 and int(item['Gid']) < 200): # read how many rows of data
        x.append(count)
        AirTemp.append(float(item['AirTemp']))
        count = count+1

z1 = np.polyfit(x,AirTemp,2)
p1= np.poly1d(z1)
yvals = p1(x)
print(p1)

ax = plt.subplot(1,1,1)
ax.plot(x,AirTemp)
ax.plot(x,yvals)

# format the ticks
ax.xaxis.set_major_locator(minutes)
ax.xaxis.set_major_formatter(minutes_fmt)
#ax.xaxis.set_minor_locator(hours)
ax.yaxis.set_major_locator(MultipleLocator(1))

# format the coords message box
ax.format_xdata = mdates.DateFormatter('%Y-%m-%d %H:%M')
ax.grid(True)

plt.show()