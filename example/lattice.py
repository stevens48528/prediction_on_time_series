# coding=UTF-8
import time
import sys
import _pickle as cPickle
from prefixTree import Node
from prefixTree import Bucket
from prefixTree import Tree

# read node object from file
f2 = open('temp.pkl', 'rb')
objs = []
while True:
    try:
        o = cPickle.load(f2)
    except EOFError:
        break
    objs.append(o)

A = objs[0]
B = objs[1]
C = objs[2]
D = objs[3]
# print ("A: " + str(A))
# print ("B: " + str(B))
# print ("C: " + str(C))
# print ("D: " + str(D))
# testdata = [(1,"A"), (2,"A"), (3,"C"), (4,"D"), (5,"A"), (6,"B"), (7,"D"), (8,"D"), (9,"B"), (10,"B")]
testdata = [(1, "A"), (2, "B"), (3,"C"), (4,"D")]
testarray = []

for item in testdata:
    # print (testarray)
    if item[-1] == "A":
        Z = A
    elif item[-1] == "B":
        Z = B
    elif item[-1] == "C":
        Z = C
    else:
        Z = D
    if item[0] != 1:
        for x in range(len(testarray)):
            tmpZ = []
            if item[0] - testarray[x]["id"] <= 2:
                for node in testarray[x]["match"]:
                    # print(node.children)
                    if isinstance(node, Node):
                        for child in list(node.children.keys()):
                            if child == item[-1]:
                                tmpZ.append(node.children[child])
                                break
                if len(tmpZ) != 0:
                    testarray.append({
                        "id": item[0],
                        "word": testarray[x]["word"] + item[-1],
                        "match": tmpZ
                    })

    testarray.append({
        "id": item[0],
        "word": item[-1],
        "match": Z
    })
    time.sleep(2)

for i in testarray:
    print ("id: " + str(i["id"]))
    print ("word: " + i["word"])
    print ("match: " + str(i["match"]))
    for g in i["match"]:
        if isinstance(g, Bucket):
            print("patterns are: " + str(g.pattern))
    print ("--------------")
