from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from collections import Counter
import numpy as np
import time

class storeDataAndCount() :
    def __init__(self, dic, count) :
        self.dic = dic
        self.count = count

def SetLogger( sc ) :
    logger = sc._jvm.org.apache.log4j
    logger.LogManager.getLogger("org"). setLevel( logger.Level.ERROR )
    logger.LogManager.getLogger("akka").setLevel( logger.Level.ERROR )
    logger.LogManager.getRootLogger().setLevel(logger.Level.ERROR)       

def time_start():
    global unitCount
    global timestart
    timestart = time.time()
    print(unitCount + 1)

def time_end():
    global timestart
    global elapsedtime
    elapsedtime = time.time() - timestart
    fo = open(timepath,"a")
    fo.write(str(elapsedtime) + "\n")
    fo.close()
    #print("")
    #print("time : " + str(elapsedtime))
    #print("")


def save_data(time, data):
    #print(str(data.collect()))
    #print("result :")
    #print(windowCounter.dic.collect())
    #print(data.collect())
    #print("")
    #fo = open(outputpath,"a")
    #fo.write(str(data) + "\n")
    #fo.close()
    data.saveAsTextFile("file://" + outputpath + str(time))

def slidedata(time, data) :
    global circularBuffer
    global windowCounter
    global unitCount
    global nextwindowend
    global windowbegin
    global windowend
    global nextunit
    
    #update unit
    unitCount += 1
    
    #window's position
    if unitCount > windowend :
        windowend = nextwindowend
        windowbegin = windowend - windowunitCount
        nextwindowend += slideunitCount
        
    #window data process
    if slideunitCount >= windowunitCount :
        if unitCount > windowbegin :
            #data to window
            words = data.flatMap(lambda line: line.split(" "))
            pairs = words.map(lambda word: (word, 1))
            counts = pairs.reduceByKey(lambda x, y : x + y)
            windowCounter.dic = windowCounter.dic.union(counts)
            
            #output window result
            if unitCount >= windowend :
                windowCounter.dic = windowCounter.dic.reduceByKey(lambda x, y : x + y)
                windowCounter.dic.checkpoint()
                windowCounter.dic = windowCounter.dic.filter(lambda elem: elem[1] >= support)
                save_data(time, windowCounter.dic)
                windowCounter.dic = sc.emptyRDD() 
    else :
        #unit data process
        words = data.flatMap(lambda line: line.split(" "))
        pairs = words.map(lambda word: (word, 1))
        counts = pairs.reduceByKey(lambda x, y : x + y)
            
        #data to circularBuffer    
        unit = nextunit
        nextunit = (nextunit + 1) % windowunitCount
        circularBuffer[unit].dic = counts
        circularBuffer[unit].count = counts.count()
        windowCounter.dic = windowCounter.dic.union(circularBuffer[unit].dic)
        
        #output window result
        if unitCount >= windowend : 
            windowCounter.dic = windowCounter.dic.reduceByKey(lambda x, y : x + y)
            windowCounter.dic = windowCounter.dic.filter(lambda elem: elem[1] >= support)
            save_data(time, windowCounter.dic)
            
            #window slide or recompute
            slidenum = 0
            slidetempnum = 0
            recomputenum = 0
            recomputetempnum = circularBuffer[(windowend - 1) % windowunitCount].count
            for item in range(0, windowunitCount) :
                slidetempnum += circularBuffer[item].count
            for item in range(0, slideunitCount) :
                slidetempnum += circularBuffer[(windowend + item) % windowunitCount].count
                slidenum += slidetempnum
            for item in range(1, reservedCount) :
                recomputetempnum += circularBuffer[(windowend - 1 - item) % windowunitCount].count
                recomputenum += recomputetempnum

            if slidenum <= recomputenum :
                for item in range(0, slideunitCount) :
                    circularBuffer[(windowend + item) % windowunitCount].dic = circularBuffer[(windowend + item) % windowunitCount].dic.mapValues(lambda x : -x)
                    windowCounter.dic = windowCounter.dic.union(circularBuffer[(windowend + item) % windowunitCount].dic)
            else :
                windowCounter.dic = sc.emptyRDD()
                for item in range(0, reservedCount) :
                    windowCounter.dic = windowCounter.dic.union(circularBuffer[(windowend - 1 - item) % windowunitCount].dic)
            windowCounter.dic.checkpoint()
    
                 
################################################################################

unitLength = 10
windowLength = 20
slideLength = 10
support = 1
outputpath = "/home/pcdm/simon/output/outputdata"
#outputpath = "hdfs://master:9000/user/hduser/output/outputdata.txt"
timepath = "/home/pcdm/simon/output/time.txt"
#timepath = "hdfs://master:9000/user/hduser/output/time.txt"
checkpointpath = "/home/pcdm/simon/checkpoint"
#checkpointpath = "hdfs://master:9000/user/hduser/checkpoint"
#textfilestreampath = "hdfs://master:9000/user/hduser/sparkDir"

################################################################################

windowunitCount = windowLength // unitLength
slideunitCount = slideLength // unitLength
reservedCount = windowunitCount - slideunitCount

sc = SparkContext("local[2]", "NetworkWordCount")

circularBuffer = np.empty((windowunitCount), Counter)
for num in range(0,windowunitCount) :
    circularBuffer[num] = storeDataAndCount(sc.emptyRDD(), 0)
unitCount = 0
windowbegin = 0
windowend = 0
nextunit = 0
nextwindowend = windowend + slideunitCount
windowCounter = storeDataAndCount(sc.emptyRDD(), 0)
timestart = 0
elapsedtime = 0


SetLogger( sc )
ssc = StreamingContext(sc, unitLength)
ssc.sparkContext.setCheckpointDir(checkpointpath)    
lines = ssc.socketTextStream("localhost", 9999)
#lines = ssc.textFileStream(textfilestreampath)
lines.foreachRDD(lambda x: time_start())
lines.foreachRDD(slidedata)
lines.foreachRDD(lambda x: time_end())


ssc.start()
ssc.awaitTermination()












