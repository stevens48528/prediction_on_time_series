import json
import datetime as dt
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter, AutoMinorLocator)

minutes = mdates.MinuteLocator(interval=30) # every 30 minutes
minutes_fmt = mdates.DateFormatter('%M')
hours = mdates.HourLocator()
hours_fmt = mdates.DateFormatter('%H')
days_fmt = mdates.DateFormatter('%d')
days = mdates.DayLocator()
months = mdates.MonthLocator()
months_fmt = mdates.DateFormatter('%m')
years = mdates.YearLocator()
years_fmt = mdates.DateFormatter('%Y')

timestamp = []
AirTemp = []
LandTemp = []

json_file = open('sensor.json')
data = json.load(json_file)
for item in data:
    if(int(item['Gid']) > 100 and int(item['Gid']) < 200): # read how many rows of data
        timestamp.append(dt.datetime.strptime(item['datatime'],"%Y-%m-%d %H:%M:%S.%f"))
        AirTemp.append(float(item['AirTemp']))
        LandTemp.append(float(item['LandTemp']))

ax = plt.subplot(1,1,1)
ax.plot(timestamp,AirTemp)
ax.plot(timestamp,LandTemp)

# format the ticks
ax.xaxis.set_major_locator(minutes)
ax.xaxis.set_major_formatter(minutes_fmt)
#ax.xaxis.set_minor_locator(hours)
ax.yaxis.set_major_locator(MultipleLocator(1))

# format the coords message box
ax.format_xdata = mdates.DateFormatter('%Y-%m-%d %H:%M')
ax.grid(True)

plt.show()