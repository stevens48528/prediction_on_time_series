import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from pyspark.ml.fpm import PrefixSpan
from pyspark import SparkContext
from pyspark.sql import SparkSession

if __name__ == "__main__":
    spark = SparkSession\
        .builder\
        .appName("PrefixSpanExample")\
        .getOrCreate()
    sc = spark.sparkContext
    #TESTFILE_PATH = "hdfs://sl-01:9000/test/A0000.csv"
    TESTFILE_PATH = "/home/pcdm/test/A0000.csv"

    dataset = pd.read_csv(TESTFILE_PATH,header=None)
    dataset.columns = ['id', 'timestamp', 'latitude', 'longitude', 'speed', 'direction']
    dataset = dataset.drop(labels=['id', 'speed'],axis='columns').to_numpy()

    train_data, test_data = train_test_split(dataset, test_size=0.2, random_state=0)

    
    df = pd.DataFrame(train_data)
    spark_df = spark.createDataFrame(df).head(20)
    df = sc.parallelize(spark_df)
    df.collect()
    prefixSpan = PrefixSpan(minSupport=0.5, maxPatternLength=5, maxLocalProjDBSize=32000000)

    # Find frequent sequential patterns.
    prefixSpan.findFrequentSequentialPatterns(df).show()
    spark.stop()