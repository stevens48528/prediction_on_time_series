# coding=UTF-8
import sys
import _pickle as cPickle

# store charactor
new_dict = {new_list: [] for new_list in list(map(chr, range(65, 69)))}

# max size of a bucket
BUCKET_SIZE = 2
# initialize the children of root
DEFAULT_CHILD = "ABC"

def printChildren(name, node, level):
    n = 1 # No. of children
    tmpList = list()
    print ("----------level " + str(level) + "-----------")
    print ("Parent: " + str(name) + " " + str(node))

    for i in node.children:
        if isinstance(node.children[i], Node):
            print ("No " + str(n) + ": Node " + i)
            new_dict[i].append(node.children[i])
            tmpList.append((node.children[i], i))
        else:
            print ("No " + str(n) + ": Bucket " + i)
            print (node.children[i].pattern)
        print("")
        n = n + 1

    if tmpList:
        for j in tmpList:
            printChildren(j[1], j[0], level + 1)

# A node mantains a dictionary with a char as key and a node or bucket as value
# addChild(k, v) --> Append a new node or new bucket as children
#   parameter k --> A char(char), v --> A node or bucket(object)
class Node:
    def __init__(self):
        self.children = dict()

    def addChild(self, k, v):
        self.children.update({k: v})

# A bucket keeps a array to store patterns
# insert(p) --> Insert a new pattern into this bucket
#   parameter p --> a pattern(str)
# sortBucket(level) --> Sort patterns by char at level.th in this bucket
#   parameter level --> the depth of this bucket
# findSplitPoint(tempList, symbol) --> Get split point
#   parameter tempList --> ,symbol -->
class Bucket:
    def __init__(self):
        self.pattern = list()

    def insert(self, p):
        self.pattern.append(p)

    def sortBucket(self, level):
        tempSymbol = ""  # keep the symbol of level
        nextSymbol = ""  # keep the symbol of next level
        tempList = list()
        for p in self.pattern:
            if not p[level-1] in tempSymbol:
                tempSymbol = tempSymbol+p[level-1]
        # if the symbols of level are the same --> record the symbol of next level
        if len(tempSymbol) == 1:
            self.pattern.sort(key=lambda x: x[level])
            for p in self.pattern:
                tempList.append(p[level])
                if not p[level] in nextSymbol:
                    nextSymbol = nextSymbol+p[level]
            # if the symbols of next level are the same too
            if len(nextSymbol) == 1:
                return tempList, nextSymbol, tempSymbol, True
        else:
            tempSymbol = ""
            self.pattern.sort()
            for p in self.pattern:
                # update the order of tempSymbol
                if not p[level-1] in tempSymbol:
                    tempSymbol = tempSymbol+p[level-1]
                tempList.append(p[level-1])
        return tempList, nextSymbol, tempSymbol, False

    def findSplitPoint(self, tempList, symbol):
        splitpoint = 0
        tmpBucketSize = 0
        if BUCKET_SIZE % 2 != 0:
            tmpBucketSize = BUCKET_SIZE + 1
        else:
            tmpBucketSize = BUCKET_SIZE
        if len(symbol) == 2:
            # Ex. [A,A,A,B,B,B] splitPoint = 3
            splitpoint = tempList.index(symbol[1])
        else:
            # the index of middle char
            index_middle = int(tmpBucketSize / 2)
            # print("index_middle is: " + str(index_middle))

            middle = tempList[index_middle]
            # print("middle is: " + middle)

            index_first_middle = tempList.index(middle)
            # print("index_first_middle is: " + str(index_first_middle))

            last_char = tempList[tempList.index(symbol[-1])]
            # print("last_char is: " + last_char)

            index_first_last_char = tempList.index(last_char)
            # print("index_first_last_char is: " + str(index_first_last_char))

            first_char = tempList[tempList.index(symbol[0])]
            # print("first_char is: " + first_char)

            index_char_after_middle = tempList.index(
                symbol[symbol.index(middle)+1])
            # print("index_char_after_middle is: " + str(index_char_after_middle))

            if middle == last_char:
                splitpoint = index_first_last_char
            elif index_char_after_middle - index_middle >= 1 and middle != first_char:
                splitpoint = index_first_middle
            else:
                splitpoint = index_middle + 1
        char = tempList[int(splitpoint)]
        return int(splitpoint), char

class Tree:
    def __init__(self):
        # default node and bucket
        self.root = Node()
        # self.root.addChild("ABCDEFGHIJKLMNOPQRSTUVWXYZ", Bucket())
        self.root.addChild(DEFAULT_CHILD, Bucket())

    def add(self, data):
        for item in data:
            level = 0
            node = self.root
            for char in item:
                is_found = False  # if found -> True
                is_leaf = False   # if find a bucket -> True
                is_match = False  # if the char is found in key -> True
                for child in list(node.children.keys()):
                    # check the char is a substring of key
                    for x in child:
                        if x == char:
                            is_match = True
                            break
                    # if match, looking for next node or inserting pattern in bucket
                    if is_match:
                        is_found = True
                        nextObject = node.children[child]
                        level = level + 1
                        # if the node point to a node
                        if isinstance(nextObject, Node):
                            node = nextObject
                            break
                        # if the node point to a bucket
                        else:
                            is_leaf = True
                            # insert if the bucket is non-empty
                            if len(nextObject.pattern) < BUCKET_SIZE:
                                nextObject.insert(item)
                                # print(nextObject.pattern)
                                break
                            # if the bucket is full then split
                            else:
                                # insert first
                                nextObject.insert(item)
                                # sort
                                """
                                判斷bucket中patterns的第level個字母都相同,和第level+1個字母相同,以此類推
                                Ex.
                                ["AACAB","AADDD"]
                                """
                                isSame = False
                                while not isSame:
                                    nextObject = node.children[child]
                                    tempList, nextSymbol, tempSymbol, is_same = nextObject.sortBucket(level)
                                    # print("tempList is : " + str(tempList))
                                    # print("nextSymbol is : " + nextSymbol)
                                    # print("tempSymbol is : " + tempSymbol)
                                    # print("is_same is : " + str(is_same))
                                    # print("child is : " + child)
                                    # print("----------------------")
                                    if (is_same):
                                        tmpNode = Node()
                                        tmpNode.addChild(nextSymbol, nextObject)
                                        if len(child) == 1:
                                            del nextObject
                                            node.addChild(tempSymbol, tmpNode)
                                        else:
                                            if tempSymbol == child[0] or tempSymbol == child[-1]:
                                                node.addChild(child.replace(
                                                    tempSymbol, ''), Bucket())
                                            else:
                                                split_at = child.index(tempSymbol)
                                                node.addChild(
                                                    child[:split_at], Bucket())
                                                node.addChild(
                                                    child[split_at+1:], Bucket())
                                            node.addChild(tempSymbol, tmpNode)
                                            del node.children[child]
                                        child = nextSymbol
                                        node = tmpNode
                                        # print(node.children)
                                        # print(node.children[child].pattern)
                                        level = level + 1
                                    else:
                                        isSame = True

                                # create new bucket
                                new_bucket = Bucket()
                                # split down to next level
                                # 當 len(child) != 1 且 第 level 個字母不相同 
                                if len(tempSymbol) != 1:
                                    # get split point
                                    splitpoint, char = nextObject.findSplitPoint(
                                        tempList, tempSymbol)
                                    # print("splitpoint is: " + str(splitpoint))
                                    # print("char is: " + char)
                                    # update node's dictionary
                                    node.addChild(
                                        child[child.index(char):], new_bucket)
                                    node.addChild(
                                        child[:child.index(char)], nextObject)
                                    # split and copy
                                    new_bucket.pattern.extend(
                                        nextObject.pattern[splitpoint:])
                                    del nextObject.pattern[splitpoint:]
                                    del node.children[child]
                                    # print(node.children)
                                    # print("char is : " + char)
                                    break

                                if len(child) != 1:
                                    if tempSymbol == child[0] or tempSymbol == child[-1]:
                                        node.addChild(child.replace(
                                            tempSymbol, ''), Bucket())
                                    else:
                                        split_at = child.index(tempSymbol)
                                        node.addChild(
                                            child[:split_at], Bucket())
                                        node.addChild(
                                            child[split_at+1:], Bucket())
                                    node.addChild(tempSymbol, nextObject)
                                    del node.children[child]
                                    child = tempSymbol
                                    # print(node.children)
                                    # print("char is : " + char)
                                    # print(node.children[char].pattern)

                                # get split point
                                splitpoint, char1 = node.children[child].findSplitPoint(
                                    tempList, nextSymbol)
                                # print("splitpoint is : " + str(splitpoint))
                                # print("char1 is : " + char1)

                                # create new node
                                new_node = Node()
                                # assign pointer to two bucket
                                new_node.addChild(
                                    nextSymbol[:nextSymbol.index(char1)], node.children[child])
                                new_node.addChild(
                                    nextSymbol[nextSymbol.index(char1):], new_bucket)
                                # split and copy
                                new_bucket.pattern.extend(
                                    node.children[child].pattern[int(splitpoint):])
                                del node.children[child].pattern[int(
                                    splitpoint):]
                                del node.children[child]
                                # update node's dictionary
                                node.children[child] = new_node
                            # process next pattern
                            break
                # char is not found then create a new bucket, and process next pattern
                if not is_found:
                    new_bucket = Bucket()
                    node.addChild(char, new_bucket)
                    # insert the pattern
                    node.children[char].insert(item)
                    break
                # process next pattern
                if is_leaf:
                    break

    def printTree(self):
        printChildren("root", self.root, 1)

    def find_prefix(self, prefix):
        node = self.root
        if not self.root.children:
            return False, prefix
        for char in prefix:
            char_not_found = True
            is_bucket = False
            for child in node.children:
                if child == char:
                    char_not_found = False
                    if isinstance(node.children[child], Node):
                        node = node.children[child]
                    else:
                        is_bucket = True
                    break
            if char_not_found:
                return False, prefix
            if is_bucket:
                return True, node.children[child].pattern
        # prefix found
   
if __name__ == "__main__":

    prefixTree = Tree()
    # testdata = ["ACAB", "BACAB", "BCBCC"]
    # testdata = ["BACAB", "BADDD", "BCBCC", "BCCCA","BBABC","BDDAD","BBAAD"]
    # testdata = ["AACAB", "BADDD", "BCBCC", "BCCCA", "CBABC", "CDDAD", "CBAAD", "AACAB", "AADDD", "ACBCC", "ACCCA", "ABABC", "ADDAD", "ABAAD", "BACAB", "BADDD", "BCBCC", "BCCCA", "BBABC", "BDDAD", "BBAAD"]
    testdata = ["AACDABBBCCCD", "AACDABBCACDCDC", "ABDBBACAAA", "CCDACBABBA", "CCDABBADDD", "CBABBDDAAA", "BBADBDAACA", "BBADBCACDD", "BBACCDABBB", "CCDABDDDAABC", "CCDABDDBACCAC"]
    # testdata = ["BADCB", "CCDDA", "ABBCD", "ABCDE", "ACABD", "DACBA"]
    prefixTree.add(testdata)
    prefixTree.printTree()

    # store node object to file
    keys = list(new_dict.keys())
    print(keys)
    keys.sort()
    f1 = open( 'temp.pkl' , 'wb' )
    for x in keys:
        cPickle.dump(new_dict[x], f1, True)
    f1.close()
    
    # read node object from file
    f2 = open('temp.pkl', 'rb')
    objs = []
    while True:
        try:
            o = cPickle.load(f2)
        except EOFError:
            break
        objs.append(o)
    print (objs[0])
    print (objs[1])
    print (objs[2])
    print(objs[3])
    
