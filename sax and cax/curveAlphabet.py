def curve2alphabet(angle,tp,ud):
    if (tp == 1):
        if (ud == "up"):
            if (angle < 45.0): return "M"
            if (angle >= 45.0 and angle < 75.0): return "N"
            if (angle >= 75.0 and angle < 105.0): return "O"
            if (angle >= 105.0 and angle < 135.0): return "P"
            if (angle >= 135.0 and angle < 150.0): return "Q"
        elif (ud == "down"):
            if (angle < 45.0): return "V"
            if (angle >= 45.0 and angle < 75.0): return "U"
            if (angle >= 75.0 and angle < 105.0): return "T"
            if (angle >= 105.0 and angle < 135.0): return "S"
            if (angle >= 135.0 and angle < 150.0): return "R"
    elif (tp == 0 and ud == "none"):
        if (angle >= 83.0 and angle < 91.0 or angle >= -91.0 and angle < -84.0): return "A"
        if (angle >= 68.0 and angle < 83.0): return "B"
        if (angle >= 53.0 and angle < 68.0): return "C"
        if (angle >= 38.0 and angle < 53.0): return "D"
        if (angle >= 23.0 and angle < 38.0): return "E"
        if (angle >= 8.0 and angle < 23.0): return "F"
        if (angle >= -8.0 and angle < 8.0): return "G"
        if (angle >= -23.0 and angle < -8.0): return "H"
        if (angle >= -38.0 and angle < -23.0): return "I"
        if (angle >= -53.0 and angle < -38.0): return "J"
        if (angle >= -68.0 and angle < -53.0): return "K"
        if (angle >= -83.0 and angle < -68.0): return "L"