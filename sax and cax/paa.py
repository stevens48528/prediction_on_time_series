"""Implements PAA."""
import numpy as np


def paa(series, paa_segments):
    """PAA implementation."""
    series_len = len(series)
    #print("series_len: " + str(series_len))

    # check for the trivial case
    if (series_len == paa_segments):
        return np.copy(series) # deep copy
    else:
        res = np.zeros(paa_segments) # 
        # check when we are even
        if (series_len % paa_segments == 0):
            inc = series_len // paa_segments # // 向下取整數
            #print("inc: " + str(inc))
            for i in range(0, series_len):
                idx = i // inc
                np.add.at(res, idx, series[i])
                # res[idx] = res[idx] + series[i]
            return res / inc # array中每個值都除以inc
        # and process when we are odd
        else:
            for i in range(0, paa_segments * series_len):
                idx = i // series_len
                pos = i // paa_segments
                np.add.at(res, idx, series[pos])
                # res[idx] = res[idx] + series[pos]
            return res / series_len
