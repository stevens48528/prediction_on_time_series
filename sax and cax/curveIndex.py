import math
import numpy as np
import warnings
import matplotlib.pyplot as plt
from curveAlphabet import curve2alphabet

warnings.simplefilter('ignore', np.RankWarning)

def find_max_distance(x,y,z):
    maxDistance = 0
    max_x = 0
    max_y = 0
    for i,j in zip(x,y):
        Distance = abs(z[0]*i-j+z[1])/math.sqrt(z[0]*z[0]+1)
        if Distance > maxDistance:
            maxDistance = Distance
            max_x = i
            max_y = j
    return(max_x, max_y)

def curve_to_string(x,y,t,l):
    outputx = []
    outputy = []
    z, res, _, _, _ = np.polyfit(x,y,1,full=True)
    # 顯示多項式
    p = np.poly1d(z)
    yvalstart = p(x[0])
    yvalsend = p(x[-1])
    # 如果誤差值大於threshold t
    if (res[0] > t):
        tp = 1
        maxError = find_max_distance(x,y,z) # 找出誤差最大的點 值為(x,y)
        #將array由誤差最大的點分為兩半
        splitIdx = x.index(maxError[0])
        X1 = x[:splitIdx+1]
        Y1 = y[:splitIdx+1]
        X2 = x[splitIdx:]
        Y2 = y[splitIdx:]
        #近似出兩段直線的斜率
        z1 = np.polyfit(X1,Y1,1)
        z2 = np.polyfit(X2,Y2,1)
        # 顯示多項式
        p1 = np.poly1d(z1)
        p1yvalstart = p1(x[0])
        p1yvalsend = p1(x[splitIdx])
        p2 = np.poly1d(z2)
        p2yvalstart = p2(x[splitIdx])
        p2yvalsend = p2(x[-1])
        #print(p1)
        #print(p2)
        # 計算夾角
        angle = round(180-math.degrees(abs(math.atan(z1[0]))+abs(math.atan(z2[0]))),3)
        # 如果角度大於170 --> 視為直線
        if (angle > 150):
            ud = "none"
            tp = 0
            print("Original degree: " + str(angle))
            angle = round(math.degrees(math.atan(z[0])),3)
            outputx.extend([x[0],x[-1]])
            outputy.extend([yvalstart,yvalsend])
        else:
            # 如果斜率一 > 斜率二 --> 開口向下
            if (z1[0] > z2[0]):
                ud = "down"
            # 如果斜率一 < 斜率二 --> 開口向上
            else:
                ud = "up"
            outputx.extend([x[0],x[splitIdx],x[splitIdx],x[-1]])
            outputy.extend([p1yvalstart,p1yvalsend,p2yvalstart,p2yvalsend])
            printSplitPoint = "Split point is: "
            print(printSplitPoint)
            print(maxError)
    else:
        angle = round(math.degrees(math.atan(z[0])),3)
        ud = "none"
        tp = 0
        outputx.extend([x[0],x[-1]])
        outputy.extend([yvalstart,yvalsend])

    symbol = curve2alphabet(angle,tp,ud)
    print("Residuals is: " + str(round(res[0],3)))
    print("Angle is: " + str(angle))
    print("Symbol is: " + symbol)
    print("")
    return symbol,outputx,outputy