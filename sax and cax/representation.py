import json
from znorm import znorm
from curveIndex import curve_to_string
from sax import ts_to_string
from paa import paa
from alphabet import cuts_for_asize
from strfunc import idx2letter
# import matplotlib.pyplot as plt
# import matplotlib.ticker as ticker

T = 0.1  # threshold 如何決定大小?
SEGMENT_SIZE = 10  # 必須大於2
ALPHABET_SIZE = 5
TOTAL_COL = 1
COL_0 = "AirTemp"


# how to count y values here
def average(item):
    sum = 0.0
    for i in [COL_0]:
        sum = float(item[i])
    return sum / TOTAL_COL


if __name__ == "__main__":
    # showfitx, showfity --> 觀察近似曲線
    showfitx = []
    showfity = []

    # x values of sequence
    x = []
    # y values of sequence
    y = []
    # Array of symbol
    cax = []
    # Where does segmentation start
    begin = 0
    # x values
    count = 1

    # Read data as sequence
    json_file = open('./sensor.json')
    data = json.load(json_file)
    for item in data:
        if(int(item['Gid']) > 1400 and int(item['Gid']) < 1501):  # read how many rows of data
            x.append(count)
            y.append(average(item))
            count = count+1
    yLength = len(y)
    remainder = yLength % SEGMENT_SIZE
    if (remainder != 0):
        y = y[:yLength - remainder]
        x = x[:yLength - remainder]
        yLength = len(y)

    # Normalization
    zn_split_y = znorm(y, 0.01)

    # CAX representation
    print("Sequence's length is: " + str(yLength))

    timesLoop = int(yLength / (SEGMENT_SIZE-1))

    print("Number of symbol: " + str(timesLoop))
    print("")

    for i in range(0, timesLoop):
        # print("---Segment " + str(i+1) + "---")
        split_x = x[begin:(begin+SEGMENT_SIZE)]
        split_y = zn_split_y[begin:(begin+SEGMENT_SIZE)]
        begin = begin + SEGMENT_SIZE-1
        # print("The segment is: ")
        # print(split_x)

        symbol, outputx, outputy = curve_to_string(
            split_x, split_y, T, SEGMENT_SIZE)
        cax.append(symbol)
        # showfitx = showfitx + outputx
        # showfity = showfity + outputy

    print("------------------------Sequence after conversion--------------------------")
    printcax = "CAX: "
    print(printcax)
    print(cax)

    # SAX representation
    cuts = cuts_for_asize(ALPHABET_SIZE)
    paa_rep = paa(zn_split_y, timesLoop)
    sax = ts_to_string(paa_rep, cuts)
    printsax = "SAX: "
    print(printsax)
    print(sax)

    # ax = plt.subplot(1,1,1)
    # ax.plot(x,zn_split_y) # 原始數據
    # ax.plot(showfitx,showfity,'--') # 近似數據
    # ax.xaxis.set_major_locator(ticker.IndexLocator(base=SEGMENT_SIZE-1, offset=0))
    # ax.axis('equal')
    # ax.grid(True)
    # plt.show()
