from __future__ import print_function
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from time import sleep
from lcssDynamic import lcs
import sys
import json

# Path
PICKLE_PATH = "/home/pcdm/test/temp.pkl"
LCSS_PATH = "/home/pcdm/test/lcssDynamic.py"
OUTPUT_PATH = "/home/pcdm/test/output.txt"
CHECKPOINT_PATH = "/home/pcdm/test/checkpoint"
TESTFILE_PATH = "hdfs://sl-01:9000/user/hduser/text"
PATTERNS_PATH = "/home/pcdm/test/patterns.json"

# Global varibal
NEW_DATA = []
patternSet = ["p1", "p2", "p3"]
PATTERNS = []
ISFIRST = True
BUFFER_LENGTH = 2
THRESHOLD = 0

# def output(data):

def process(input):
    global THRESHOLD
    global patternSet
    global NEW_DATA
    global ISFIRST

    data = input.collect()
    if ISFIRST:
        NEW_DATA.append(data[0])
        if len(NEW_DATA) > BUFFER_LENGTH:
            ISFIRST = False
    else:
        NEW_DATA = []
        NEW_DATA.append(data[0])
    if not ISFIRST:
        score = lcs(NEW_DATA, PATTERNS[0][patternSet[-1]])
        if (score < THRESHOLD):
            patternSet.pop()
            if len(patternSet) == 0:
                print("empty!")
        else:
            print("Score is: " + str(score))
            print("Pattern is: " + patternSet[-1])

if __name__ == "__main__":
    # Set up the Spark context and the streaming context
    """
    if len(sys.argv) != 3:
        print("Usage: latticeSpark.py <hostname> <port>", file=sys.stderr)
        sys.exit(-1)
    """

    sc = SparkContext("local[*]", appName="test")
    ssc = StreamingContext(sc, 2)
    # ssc.checkpoint(CHECKPOINT_PATH)
    sc.addPyFile(LCSS_PATH)
    
    # Import patterns
    json_file = open(PATTERNS_PATH)
    data = json.load(json_file)
    for item in data:
        PATTERNS.append(item)

    # Input data
    rddQueue = []
    test = [
        {
            "id": "1",
            "datatime": "2018-12-21 11:13:21.000000",
            "AirTemp": "23",
            "AirHumi": "23",
            "LandTemp": "25",
            "LandHumi": "23"
        },
        {
            "id": "2",
            "datatime": "2018-12-21 13:00:57.000000",
            "AirTemp": "23",
            "AirHumi": "24",
            "LandTemp": "20",
            "LandHumi": "25"
        },
        {
            "id": "3",
            "datatime": "2018-12-21 13:15:54.000000",
            "AirTemp": "29.8",
            "AirHumi": "31",
            "LandTemp": "20",
            "LandHumi": "24"
        },
        {
            "id": "4",
            "datatime": "2018-12-21 13:30:54.000000",
            "AirTemp": "24",
            "AirHumi": "30",
            "LandTemp": "22",
            "LandHumi": "24"
        },
        {
            "id": "5",
            "datatime": "2018-12-21 13:45:54.000000",
            "AirTemp": "25",
            "AirHumi": "29",
            "LandTemp": "22",
            "LandHumi": "24"
        },
        {
            "id": "6",
            "datatime": "2018-12-21 14:00:54.000000",
            "AirTemp": "26",
            "AirHumi": "31",
            "LandTemp": "21",
            "LandHumi": "25"
        },
        {
            "id": "7",
            "datatime": "2018-12-21 14:15:52.000000",
            "AirTemp": "26",
            "AirHumi": "70",
            "LandTemp": "21",
            "LandHumi": "54"
        },
        {
            "id": "8",
            "datatime": "2018-12-21 14:30:50.000000",
            "AirTemp": "25",
            "AirHumi": "72",
            "LandTemp": "21",
            "LandHumi": "54"
        },
        {
            "id": "9",
            "datatime": "2018-12-21 14:45:49.000000",
            "AirTemp": "25",
            "AirHumi": "74",
            "LandTemp": "21",
            "LandHumi": "54"
        },
        {
            "id": "10",
            "datatime": "2018-12-21 15:00:49.000000",
            "AirTemp": "25",
            "AirHumi": "76",
            "LandTemp": "21",
            "LandHumi": "54"
        }
    ]
    for j in test:
        rddQueue += [ssc.sparkContext.parallelize([j])]

    inputStream = ssc.queueStream(rddQueue)
    # inputStream = ssc.textFileStream(TESTFILE_PATH)
    # inputStream = ssc.socketTextStream("localhost", 9999)
    # inputStream = inputStream.map(lambda x: json.loads(x))
    inputStream.map(lambda x: "Input: " + str(x)).pprint()

    inputStream.foreachRDD(process)

    ssc.start()
    sleep(40)
    ssc.stop(stopSparkContext=True, stopGraceFully=True)

    # ssc.start()
    # ssc.awaitTermination()