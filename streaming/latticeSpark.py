from __future__ import print_function
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from time import sleep
from prefixTree import Node
from prefixTree import Bucket
from prefixTree import Tree
import cPickle as pickle
import sys

# path
PICKLE_PATH = "/home/pcdm/test/temp.pkl"
PREFIXTREE_PATH = "/home/pcdm/test/prefixTree.py"
OUTPUT_PATH = "/home/pcdm/test/output.txt"
CHECKPOINT_PATH = "/home/pcdm/test/checkpoint"
TESTFILE_PATH = "/home/pcdm/test/test.txt"

def output(data):
    global IDX
    global THRESHOLD
    if data[1]["id"] == IDX[0]:
        for i in data[1]["match"]:
            if isinstance(i, Bucket) and len(data[1]["word"]) > THRESHOLD:
                fo = open(OUTPUT_PATH,"a")
                fo.write("Word: " + data[1]["word"] + "\n")
                fo.write("Patterns are: " + str(i.pattern) + "\n")
                fo.write("\n")
def find(data):
    global WORD
    global IDX
    tmpZ = []
    for node in data["match"]:
        if isinstance(node, Node):
            for child in list(node.children.keys()):
                if child == WORD[0]:
                    tmpZ.append(node.children[child])
                    break
    if len(tmpZ) != 0:
        return {
            "id": IDX[0],
            "word": data["word"] + WORD[0],
            "match": tmpZ,
            "ismatch": True
        }
    else:
        return {
            "id": data["id"],
            "word": data["word"],
            "match": data["match"],
            "ismatch": False
        }
def process(input):
    global result
    global WORD
    global IDX

    WORD = input.values().collect()
    IDX = input.keys().collect()

    if WORD[0] == "A":
        Z = bA.value
    elif WORD[0] == "B":
        Z = bB.value
    elif WORD[0] == "C":
        Z = bC.value
    else:
        Z = bD.value

    input = input.mapValues(lambda x: {"id": IDX[0], "word": WORD[0], "match": Z})
    old_result = sc.emptyRDD() \
            .union(result)
    result = result.filter(lambda x: IDX[0] - x[1]["id"] <= 2) \
            .mapValues(find) \
            .filter(lambda  x: x[1]["ismatch"] == True)
    fo = open(OUTPUT_PATH,"a")
    fo.write("time: " + str(IDX[0]) + "\n")
    result.foreach(output)
    # union
    result = result.union(input) \
            .union(old_result)
    # print
    # print result.sortByKey().collect()
    # unpersist
    old_result.unpersist()
    input.unpersist()

if __name__ == "__main__":
    # Set up the Spark context and the streaming context
    '''
    if len(sys.argv) != 3:
        print("Usage: latticeSpark.py <hostname> <port>", file=sys.stderr)
        sys.exit(-1)
    '''

    sc = SparkContext("local[*]", appName="test")
    ssc = StreamingContext(sc, 2)
    # ssc.checkpoint(CHECKPOINT_PATH)
    sc.addPyFile(PREFIXTREE_PATH)
    
    # global varibal
    THRESHOLD = 3
    result = sc.emptyRDD()
    WORD = []
    IDX = []

    # read node object from file
    f2 = file(PICKLE_PATH, 'rb')
    objs = []
    while True:
        try:
            o = pickle.load(f2)
        except EOFError:
            break
        objs.append(o)
    bA = sc.broadcast(objs[0])
    bB = sc.broadcast(objs[1])
    bC = sc.broadcast(objs[2])
    bD = sc.broadcast(objs[3])

    # Input data
    rddQueue = []
    words = ["A", "B", "C", "D"]
    i = 1
    for j in words:
        rddQueue += [ssc.sparkContext.parallelize([(i, j)])]
        i = i + 1
    inputStream = ssc.queueStream(rddQueue)
    # inputStream = ssc.textFileStream(TESTFILE_PATH)
    # inputStream = ssc.socketTextStream("localhost", 9999)
    inputStream.map(lambda x: "Input: " + str(x)).pprint()

    inputStream.foreachRDD(process)

    ssc.start()
    ssc.stop(stopSparkContext=True, stopGraceFully=True)

    # ssc.start()
    # ssc.awaitTermination()