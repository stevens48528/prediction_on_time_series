def findSplitPoint(symbol, level):
    bucket_size = 6
    pattern = ["AACABA", "ACDDDA", "ABBCCA", "BACCAA","BCABCA","DDDADA","DDAADA"]
    splitpoint = 0
    tempList = list()
    if len(symbol) == 1:
        symbol = ""
        pattern.sort(key = lambda x: x[level])
        print(pattern)
        for p in pattern:
            tempList.append(p[level])
            if not p[level] in symbol:
                symbol = symbol+p[level]
        print(symbol)
    else:
        pattern.sort()
        for p in pattern:
            tempList.append(p[level-1])
    print(tempList)
    if len(symbol) == 2:
        splitpoint = tempList.index(symbol[1])-1 # Ex. [A,A,A,B,B,B] splitPoint = 3
        char = symbol[1]
    else:
        middle = tempList[int(bucket_size/2-1)]
        print(middle)
        indexof_middle = tempList.index(middle)
        last_char_index = tempList.index(symbol[-1])
        charindex_after_middle = tempList.index(symbol[symbol.index(middle)+1])
        if middle == last_char_index:
            splitpoint = bucket_size/2-1
            char = middle
        elif (charindex_after_middle-indexof_middle) == 1:
            splitpoint = bucket_size/2
            char = tempList[splitpoint]
        else:
            splitpoint = charindex_after_middle
            char = symbol[symbol.index(middle)+1]
    print(splitpoint)

findSplitPoint("ABC", 1)