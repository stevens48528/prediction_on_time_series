import pandas as pd
import time
import ciso8601

def toTimeStamp(x):
    t = ciso8601.parse_datetime(x)
    return time.mktime(t.timetuple())

data = pd.read_json('./sensor.json')
data.drop(['Gid', 'portid'], axis=1, inplace=True)
data['datatime'] = data['datatime'].apply(lambda x: int(toTimeStamp(x)))
print(data.head(20))

max = 0
min = 0