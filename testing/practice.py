import numpy as np

a=np.array((1,2,3,4,5))# 引數是元組 
b=np.array([6,7,8,9,0])# 引數是list 
c=np.array([[1,2,3],[4,5,6]])# 引數二維陣列 
print(a)
print(b)

print(np.shape(c))