import math
def find_max_distance(x,y,z):
    maxDistance = 0
    max_x = 0
    max_y = 0
    for i,j in zip(x,y):
        Distance = abs(z[0]*i-j+z[1])/math.sqrt(z[0]*z[0]+1)
        if Distance > maxDistance:
            maxDistance = Distance
            max_x = i
            max_y = j
    return(max_x, max_y)

z = [0,1]
x = [0,3,5]
y = [0,5,0]

max = find_max_distance(x,y,z)

print(max)