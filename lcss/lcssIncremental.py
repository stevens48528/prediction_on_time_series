import math
import datetime as dt

# Global Varible
THRESHOLD1 = 2
THRESHOLD2 = 4
W1 = 1
W2 = 0
T = 15  # minute

# init
timeDiff = 0
N = 0
distance = 0
CopyL = []

# Data Attribute
TOTAL_COL = 1
COL_0 = "AirTemp"

# Euclidean distance
def d(X, Y):
    sum = 0.0
    for i in [COL_0]:
        sum = sum + math.pow(float(X[i])-float(Y[i]), 2)
    return float(math.sqrt(sum))


def localScore(d, time):
    return W1 * d + W2 * time


def localScore2(distance):
    return round((THRESHOLD2-distance)/(THRESHOLD2-THRESHOLD1), 2)


def lcs(X, Y):
    global CopyL
    # find the length of the strings
    m = len(X)
    n = len(Y)

    # declaring the array for storing the dp values
    L = [[None]*(n+1) for i in range(m+1)]
    # Insert previous result
    if m == 1:
        for j in range(n+1):
            L[0][j] = CopyL[0]
            del CopyL[0]

    """Following steps build L[m+1][n+1] in bottom up fashion 
    Note: L[i][j] contains length of LCS of X[0..i-1] 
    and Y[0..j-1]"""
    for i in range(m+1):
        for j in range(n+1):
            if i != 0 and j != 0:
                global timeDiff
                global distance
                global N
                t1 = dt.datetime.strptime(
                    X[i - 1]['datatime'], "%Y-%m-%d %H:%M:%S.%f")
                t2 = dt.datetime.strptime(
                    Y[j - 1]['datatime'], "%Y-%m-%d %H:%M:%S.%f")
                timeDiff = abs((t1.hour*60+t1.minute)-(t2.hour*60+t2.minute))
                distance = d(X[i-1], Y[j-1])
                # print("t is: " + str(timeDiff))
                # print("N is: " + str(lenDiff))
                # print("distance is: " + str(distance))
                if distance <= THRESHOLD1 and timeDiff <= T:
                    L[i][j] = L[i-1][j-1] + localScore(distance, timeDiff)
                elif THRESHOLD1 < distance <= THRESHOLD2 and timeDiff <= T:
                    L[i][j] = max(L[i-1][j-1] + localScore2(distance),
                                  L[i - 1][j], L[i][j - 1])
                else:
                    L[i][j] = max(L[i-1][j], L[i][j-1])
                # print(i, j)
                # print(L[i][j])
                # print("-----------------")
            else:
                if m == 1 and i == 0:
                    break
                else:
                    L[i][j] = 0
    # print('\n'.join([''.join(['{:4}'.format(item) for item in row])
                     # for row in L]))
    CopyL = []
    for k in range(n+1):
        CopyL.append(L[-1][k])
    # print(CopyL)
    # L[m][n] contains the length of LCS of X[0..n-1] & Y[0..m-1]
    # print("Answer is: ")
    return L[m][n]
    # end of function lcs


if __name__ == "__main__":
    '''
    X = [
        {
            "id": "2",
            "datatime": "2018-12-21 13:00:57.000000",
            "AirTemp": "23",
            "AirHumi": "24",
            "LandTemp": "20",
            "LandHumi": "25"
        },
        {
            "id": "3",
            "datatime": "2018-12-21 13:15:54.000000",
            "AirTemp": "29.8",
            "AirHumi": "31",
            "LandTemp": "20",
            "LandHumi": "24"
        },
        {
            "id": "4",
            "datatime": "2018-12-21 13:50:54.000000",
            "AirTemp": "24",
            "AirHumi": "30",
            "LandTemp": "22",
            "LandHumi": "24"
        }
    ]
    Y = [
        {
            "id": "5",
            "datatime": "2018-12-21 11:20:54.000000",
            "AirTemp": "25",
            "AirHumi": "29",
            "LandTemp": "22",
            "LandHumi": "24"
        },
        {
            "id": "6",
            "datatime": "2018-12-21 13:15:54.000000",
            "AirTemp": "26",
            "AirHumi": "31",
            "LandTemp": "21",
            "LandHumi": "25"
        },
        {
            "id": "7",
            "datatime": "2018-12-21 13:20:52.000000",
            "AirTemp": "26",
            "AirHumi": "70",
            "LandTemp": "21",
            "LandHumi": "54"
        },
        {
            "id": "8",
            "datatime": "2018-12-21 13:30:50.000000",
            "AirTemp": "25",
            "AirHumi": "72",
            "LandTemp": "21",
            "LandHumi": "54"
        }
    ]
    CopyX = []
    # print("Answer is: ")
    for i in X:
        CopyX = []
        CopyX.append(i)
        print(lcs(CopyX, Y))
    '''
