import math
import datetime as dt

THRESHOLD1 = 2
THRESHOLD2 = 4
W1 = 1
W2 = 0
T = 15  # minute
DELTA = 5

# init
timeDiff = 0
distance = 0

TOTAL_COL = 1
COL_0 = "AirTemp"

# Euclidean distance
def d(X, Y):
    sum = 0.0
    for i in [COL_0]:
        sum = sum + math.pow(float(X[i])-float(Y[i]), 2)
    return float(math.sqrt(sum))


def localScore(d, time):
    return W1 * d + W2 * time


def localScore2(distance):
    return round((THRESHOLD2-distance)/(THRESHOLD2-THRESHOLD1), 2)

def lcs(X, Y):
    # find the length of the strings
    m = len(X) # 4
    n = len(Y) # 3

    # declaring the array for storing the dp values
    L = [[None]*(n+1) for i in range(m+1)]

    """Following steps build L[m+1][n+1] in bottom up fashion 
    Note: L[i][j] contains length of LCS of X[0..i-1] 
    and Y[0..j-1]"""
    for i in range(m+1):
        for j in range(n+1):
            if i != 0 and j != 0:
                global timeDiff
                global distance
                t1 = dt.datetime.strptime(
                    X[i - 1]['datatime'], "%Y-%m-%d %H:%M:%S.%f")
                t2 = dt.datetime.strptime(
                    Y[j - 1]['datatime'], "%Y-%m-%d %H:%M:%S.%f")
                timeDiff = abs((t1.hour*60+t1.minute)-(t2.hour*60+t2.minute))
                distance = d(X[i-1], Y[j-1])
                print("t is: " + str(timeDiff))
                print("distance is: " + str(distance))

            if i == 0 or j == 0:
                L[i][j] = 0
            elif distance <= THRESHOLD1 and timeDiff <= T:
                L[i][j] = L[i-1][j-1] + localScore(distance, timeDiff)
            elif THRESHOLD1 < distance <= THRESHOLD2 and timeDiff <= T:
                L[i][j] = max(L[i-1][j-1] + localScore2(distance),
                              L[i - 1][j], L[i][j - 1])
            else:
                L[i][j] = max(L[i-1][j], L[i][j-1])
            print(i, j)
            print(L[i][j])
            print("-----------------")

    # L[m][n] contains the length of LCS of X[0..n-1] & Y[0..m-1]
    # print("Answer is: ")
    return L[m][n]
    # end of function lcs


if __name__ == "__main__":
    X = [
        {
            "datatime": "2018-12-21 11:13:21.000000",
            "AirTemp": "23"
        },
        {
            "datatime": "2018-12-21 13:00:57.000000",
            "AirTemp": "23"
        },
        {
            "datatime": "2018-12-21 13:15:54.000000",
            "AirTemp": "29.8"
        },
        {
            "datatime": "2018-12-21 13:30:54.000000",
            "AirTemp": "24"
        },
        {
            "datatime": "2018-12-21 13:45:54.000000",
            "AirTemp": "25"
        },
        {
            "datatime": "2018-12-21 14:00:54.000000",
            "AirTemp": "26"
        },
        {
            "datatime": "2018-12-21 14:15:52.000000",
            "AirTemp": "26"
        },
        {
            "datatime": "2018-12-21 14:30:50.000000",
            "AirTemp": "25"
        },
        {
            "datatime": "2018-12-21 14:45:49.000000",
            "AirTemp": "25"
        },
        {
            "datatime": "2018-12-21 15:00:49.000000",
            "AirTemp": "25"
        }
    ]
    Y = [
        {
        "datatime": "2018-12-21 11:13:21.000000",
        "AirTemp": "23.5"
      },
      {
        "datatime": "2018-12-21 13:00:57.000000",
        "AirTemp": "31.2"
      },
      {
        "datatime": "2018-12-21 13:15:54.000000",
        "AirTemp": "29.8"
      },
      {
        "datatime": "2018-12-21 13:30:54.000000",
        "AirTemp": "28.5"
      },
      {
        "datatime": "2018-12-21 13:45:54.000000",
        "AirTemp": "27.1"
      }
    ]
    # print("Answer is: ")
    print(lcs(X, Y))